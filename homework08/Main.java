package com.company;

public class Main {

    public static void main(String[] args) {

        Human[] humans = new Human[10];
        humans[0] = new Human ("Slava", 100);
        humans[1] = new Human ("Daniil", 71);
        humans[2] = new Human ("Slav", 50.1);
        humans[3] = new Human ("Dan", 90);
        humans[4] = new Human ("Sla", 120);
        humans[5] = new Human ("D", 50);
        humans[6] = new Human ("Sl", 60);
        humans[7] = new Human ("Dг", 45);
        humans[8] = new Human ("Dг", 75);
        humans[9] = new Human ("Ser", 110);

        for (int i = 0; i < humans.length; i++) {
            double min = humans[i].getWeight();
            int minIndex = i;
            for (int j = i + 1; j < humans.length; j++) {
                if (humans[j].getWeight() < min) {
                    min = humans[j].getWeight();
                    minIndex = j;
                    }
                }
                Human temp = humans[i];
                humans[i] = humans[minIndex];
                humans[minIndex] = temp;
                }
            for (int i = 0; i<humans.length;i++) {
            System.out.println(humans[i].name + " " + humans[i].getWeight());
            }
    }
}


